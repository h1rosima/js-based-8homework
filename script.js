//1
let myArray = ["travel", "hello", "eat", "ski", "lift"];
let count = myArray.filter(element => element.length > 3).length;
console.log("Кол-во элементов в массиве, что имеют длинну больше 3-х символов:", count);

//2
let dataArray = [
    {
        name: `Ivan`,
        age: 30,
        sex: `man`,
    },
    {
        name: `Olha`,
        age: 31,
        sex: `woman`,
    },
    {
        name: `Julia`,
        age: 26,
        sex: `woman`,
    },
    {
        name: `Alex`,
        age: 24,
        sex: `man`,
    },
]
const filteredDataArray = dataArray.filter(person=> person.sex === `man`) ;
console.log(filteredDataArray);

//3
function filterBy(array, dataType) {
    return array.filter(item => typeof item !== dataType);
}

let data = ['hello', 'world', 23, '23', null];
let filteredData = filterBy(data, 'string');
console.log(filteredData);
